Pagination widget via js
===============================

requirements
------------

- jQuery

usage (see index.html)
-----

- include pagination.js
- add <div class="pagination" data-pages="10"></div> where you want to see paginator
- specify pages count by data-pages attribute
